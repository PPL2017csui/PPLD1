class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]

# GET /events
# GET /events.json
  def index
    @events = Event.where(start: params[:start]..params[:end]).where("user_id='#{get_oauth_user['result']['phid']}' OR tag @> ARRAY['#{get_oauth_user['result']['userName']}']::text[]")
  end

# GET /events/1
# GET /events/1.json
  def show
  end

# GET /events/new
  def new
    @event = Event.new
  end

# GET /events/1/edit
  def edit
  end

# POST /events.json
  def create
    @event = Event.new(event_params)

    if @event.valid?
      if is_overlaping
        render :json => { error_code: EventsError.code(1), error_message: EventsError.message(1) }, :status => :not_acceptable
      else
        @event.save
      end
    else
      render :json => { error_code: EventsError.code(2), error_message: EventsError.message(2) }, :status => :not_acceptable
    end
  end

# PATCH/PUT /events/1.json
  def update
    if @event.user_id != get_oauth_user['result']['phid']
      render :json => { error_code: EventsError.code(3), error_message: EventsError.message(3) }, :status => :not_acceptable
    elsif is_overlaping
      render :json => { error_code: EventsError.code(1), error_message: EventsError.message(1) }, :status => :not_acceptable
    else
      unless @event.update(event_params)
        render :json => { error_code: EventsError.code(2), error_message: EventsError.message(2) }, :status => :not_acceptable
      end
    end
  end

# DELETE /events/1.json
  def destroy
    if @event.user_id != get_oauth_user['result']['phid']
      render :json => { error_code: EventsError.code(3), error_message: EventsError.message(3) }, :status => :not_acceptable
    else
      @event.destroy
    end
  end

  private
    def is_overlaping
      before_start = nil
      before_end = nil

      event_start = params[:event][:start].to_time unless params[:event][:start].nil? rescue @event.start
      event_end = params[:event][:end].to_time unless params[:event][:end].nil? rescue @event.end

      get_users_events.each do |e|
        if e.start <= event_start
          before_start = e.end if before_start.nil? || before_start < e.end
        elsif e.start < event_end
          before_end = e.end if before_end.nil? || before_end < e.end
        end
      end unless get_users_events.nil?

      before_start = event_start - 1.second if before_start.nil?
      return before_start > event_start || before_end != nil
    end

    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      params[:event][:user_id] = get_oauth_user['result']['phid'] if params[:event][:user_id].nil?
      params.require(:event).permit(:title, :date_range, :start, :end, :color, :task_id, :desc, :user_id, :project_id, :manual_date, :tag => [])
    end

    def get_users_events
      tagged_phids = []
      tagged_usernames = []

      clean_tag = []
      clean_tag = params[:event][:tag].keep_if { |u| u.empty? == false } unless params[:event][:tag].nil?
      clean_tag = [*clean_tag, *@event.tag].uniq

      tagged_users = @conduit.user.search(constraints: { usernames: clean_tag }) unless clean_tag.nil? || clean_tag.empty?

      tagged_users['result']['data'].each do |u|
        tagged_phids << u['phid']
        tagged_usernames << u['fields']['username']
      end unless tagged_users.nil?

      phids = [get_oauth_user['result']['phid'], *tagged_phids].uniq
      usernames = [get_oauth_user['result']['userName'], *tagged_usernames].uniq

      if @event.new_record?
        return Event.where("user_id IN (?) OR tag && ARRAY[?]::text[]", phids, usernames)
      else
        return Event.where("user_id IN (?) OR tag && ARRAY[?]::text[]", phids, usernames).where.not(id: @event.id)
      end
    end
end
