require 'rails_helper'

RSpec.describe "custom_tasks/show", type: :view do
  before(:each) do
    @custom_task = assign(:custom_task, CustomTask.create!(
      :title => "Title",
      :color => "Color"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Color/)
  end
end
