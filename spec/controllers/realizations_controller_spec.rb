require 'rails_helper'

RSpec.describe RealizationsController, type: :controller do

before :each do
		request.headers["accept"] = 'application/javascript'
	end

	let(:valid_attributes) {
    	{
    		task_id: "TASK_123",
    		title: "INI TASK SEMU", 
    		user_id: "USER_123",
    		project_id: "PROJ_123",
    		start: "2017-03-30 13:00",
    		end: "2017-03-30 14:00"
    	}
  	}

	describe "GET #index" do
		it "realizations retrieved correctly" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
	      	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)
			realization = Realization.new(valid_attributes)
			realization.save
			get :index, params: {start: "2017-03-30 00:00", end: "2017-03-30 23:59", format: :json}
			expect(response.content_type).to eq("application/json")
			expect(assigns(:realizations)).to eq([realization])
		end
	end

end
