require "rails_helper"

RSpec.describe CustomTasksController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/custom_tasks").to route_to("custom_tasks#index")
    end

    it "routes to #new" do
      expect(:get => "/custom_tasks/new").to route_to("custom_tasks#new")
    end

    it "routes to #show" do
      expect(:get => "/custom_tasks/1").to route_to("custom_tasks#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/custom_tasks/1/edit").to route_to("custom_tasks#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/custom_tasks").to route_to("custom_tasks#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/custom_tasks/1").to route_to("custom_tasks#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/custom_tasks/1").to route_to("custom_tasks#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/custom_tasks/1").to route_to("custom_tasks#destroy", :id => "1")
    end

  end
end
