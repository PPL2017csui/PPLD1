require 'rails_helper'

RSpec.describe ScheduleController, type: :controller do
	let(:valid_attributes) {
		{
			user_id: "PHID-USER-qhcz6q6vyne6c6mq3yys", 
		 	task_id: "PHID-TASK-bpj4ksllh4hxsycxvj5i",
		 	project_id: "PHID-PROJ-b4h6mzo7qmbiophwyylv",
		 	date: "#{Time.now.year}/#{Time.now.month}/#{Time.now.day}",
		 	time_start: "09.00",
		 	time_end: "10.00"
		}
	}

	describe "GET daily" do
		it "assign all tasks scheduled for today as @schedules" do
			allow(controller).to receive(:user_signed_in?).and_return(true);
			schedule = Schedule.create! valid_attributes
			get :daily, params: {date: "#{Time.now.year}/#{Time.now.month}/#{Time.now.day}"}
			expect(Schedule.where({date: "#{Time.now.year}/#{Time.now.month}/#{Time.now.day}"})).to eq([schedule])
		end
	end

	describe "POST save_schedule" do
		it "replacing the old schedule with the new schedule" do
			allow(controller).to receive(:user_signed_in?).and_return(true);
			expect {
				post :save_schedule, params: {schedule: valid_attributes}
			}.to change(Schedule.where({date: "#{Time.now.year}/#{Time.now.month}/#{Time.now.day}"}), :count).to(1)
		end
	end
end