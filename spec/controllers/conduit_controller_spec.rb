require 'rails_helper'

RSpec.describe ConduitController, type: :controller do
	let(:valid_attributes) {
		{
			task_id: "TASK_123",
			title: "Task", 
			user_id: "PHID-USER-qhcz6q6vyne6c6mq3yys",
			project_id: "PROJ_123",
			start: "2017-03-30 13:00",
			end: "2017-03-30 14:00"
		}
	}

	describe "GET #search_task" do
		it "responds to json by default" do
			get :search_task, params: {context: "T1234"}, session: { access_token: "random_key" } 
			expect(response.content_type).to eq("application/json");
		end
	end

	describe "GET #search_user with username" do
		it "responds to json by default" do
			get :search_user, params: {name_like: "dayat"}, session: { access_token: "random_key" }
			expect(response.content_type).to eq("application/json");
		end
	end

	describe "GET #search_user with phid users" do
		it "responds to json by default" do
			get :search_user, params: {members_id: ["PHID-USER-5mzwgwhcoiyzi7zgt5yf"], phid: "PHID-PROJ-s6uxkfbl4z4as3rkb4xq"}, session: { access_token: "random_key" }
			expect(response.content_type).to eq("application/json");
		end
	end

	describe "GET #search_project" do
		it "responds to json by default" do
			get :search_project, params: {name_like: "productivity"}, session: { access_token: "random_key" }
			expect(response.content_type).to eq("application/json");
		end
	end

	describe "GET #get_all_events" do
		it "responds to json by default" do
			new_event = Event.create! valid_attributes
			get :get_all_events, params: {users_id: ["PHID-USER-qhcz6q6vyne6c6mq3yys"]}, session: { access_token: "random_key" }
			expect("Task").to eq(new_event.title);
			expect(response.content_type).to eq("application/json");
		end
	end
end