require 'rails_helper'
require 'uri'

RSpec.describe ApplicationController, type: :controller do
	let(:valid_attributes) {
		{
			task_id: "TASK_123",
			title: "INI TASK SEMU",
			user_id: "USER_123",
			project_id: "PROJ_123",
			start: "2017-03-30 13:00 +0700",
			end: "2017-03-30 14:00 +0700"
		}
	}

	describe "#business_hour_check" do
		it "returns 7 days of user settings" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
    	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)
			subject.send(:business_hour_check)
			settings = Setting.where(user_id: "USER_123")
			expect(settings.count()).to eq(7)
		end
	end

	describe "#oauth_url" do
		it "returns oauthserver code request url" do
			expect(subject.oauth_url).to include("#{ENV['PHHOST']}/oauthserver/auth/?response_type=code&client_id=")
		end
	end

	describe "#oauth_server_uri" do
		it "returns oauth server uri" do
			expect(subject.oauth_server_uri).to eq(URI(ENV['PHHOST']))
		end
	end

	describe "#get_user_running_task" do
		it "current_user_task on event_id" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
    	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)

			event = Event.create! valid_attributes
			current_task = User.create! ({phid: "USER_123", event_id: event.id, task_id: event.task_id})

			expect(subject.get_user_running_task.title).to be == event.title
		end

		it "current_user_task on task_id" do
			subject.send(:create_conduit_instance)
			allow(controller).to receive(:user_signed_in?).and_return(true)
    	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)

			current_task = User.create! ({phid: "USER_123", task_id: 1})

			expect(subject.get_user_running_task.title).to match(/\(1\):/)
		end
	end

end
