class CustomTasksController < ApplicationController
  before_action :set_custom_task, only: [:show, :edit, :update, :destroy]
  before_action :authenticate!

  # GET /custom_tasks
  # GET /custom_tasks.json
  def index
    @custom_tasks = CustomTask.all
  end

  # GET /custom_tasks/1
  # GET /custom_tasks/1.json
  def show
    @custom_tasks = CustomTask.find(params[:id])
    render action: 'index'
  end

  # GET /custom_tasks/new
  def new
    @custom_task = CustomTask.new
  end

  # GET /custom_tasks/1/edit
  def edit
  end

  # POST /custom_tasks
  # POST /custom_tasks.json
  def create
    @custom_task = CustomTask.new(custom_task_params)
    respond_to do |format|
      if @custom_task.save
        format.html { redirect_to schedule_url, notice: 'Custom task was successfully created.' }
        format.json { render :show, status: :created, location: @custom_task }
      else
        format.html { render :new }
        format.json { render json: @custom_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /custom_tasks/1
  # PATCH/PUT /custom_tasks/1.json
  def update

  end

  # DELETE /custom_tasks/1
  # DELETE /custom_tasks/1.json
  def destroy
    @custom_task.destroy
    respond_to do |format|
      format.html { redirect_to schedule_url, notice: 'Custom task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_custom_task
      @custom_task = CustomTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def custom_task_params
      params.require(:custom_task).permit(:title, :color)
    end
end
