require 'rails_helper'

RSpec.describe SearchController, type: :controller do

	describe "GET #search" do
		it "returns http success" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
			get :search
			expect(response).to have_http_status(:success)
		end
	end
end
