class CreateSchedules < ActiveRecord::Migration[5.0]
  def change
    create_table :schedules do |t|
      t.string :user_id
      t.string :task_id
      t.string :project_id
      t.string :date
      t.string :time_start
      t.string :time_end

      t.timestamps
    end
  end
end
