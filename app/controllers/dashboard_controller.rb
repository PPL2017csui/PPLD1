class DashboardController < ApplicationController
	before_action :authenticate!

  def daily
    @events = Realization.where(start: params[:start]..params[:end])
    render action: 'daily'
  end

  def progress
    render action: 'progress'
  end

  def update_end_time
    running_task = get_user_running_task
    if running_task.nil?
      raise DashboardError, print_error(DashboardError.code(2), DashboardError.message(2))
    else
      current_user = User.find_or_create_by(phid: get_oauth_user['result']['phid'])
      current_user.end = current_user.end + params[:minutes].to_i.minute

      current_user.save

      redirect_to '/dashboard'
    end
  end

  def start_task
    running_task = get_user_running_task
    unless running_task.nil?
      raise DashboardError, print_error(DashboardError.code(1), DashboardError.message(1))
    else
      current_user = User.find_or_create_by(phid: get_oauth_user['result']['phid'])
      current_user.task_id = params[:task_id]
      current_user.start = Time.now
      current_user.end = Time.now - 1.second

      current_user.save

      redirect_to '/dashboard'
    end
  end

  def start_event
		running_task = get_user_running_task
    unless running_task.nil?
      redirect_to '/dashboard', :flash => { error_code: DashboardError.code(1), error_message: DashboardError.message(1) }
    else
      event = Event.find(params[:event_id])
      current_user = User.find_or_create_by(phid: get_oauth_user['result']['phid'])
      current_user.event_id = event.id
      current_user.task_id = event.task_id
      current_user.start = Time.now
      current_user.end = event.end
      if event.end.to_i < Time.now.to_i
        current_user.end = Time.now - 1.second
      end

      current_user.save

      redirect_to '/dashboard'
    end
  end

  def end_running_task
		running_task = get_user_running_task
    if running_task.nil?
      raise DashboardError, print_error(DashboardError.code(2), DashboardError.message(2))
    else
      current_user = User.find_or_create_by(phid: get_oauth_user['result']['phid'])

      realization = Realization.new do |r|
				r.task_id = current_user.task_id
        r.user_id = get_oauth_user['result']['phid']
				r.title = running_task.title
        r.start = current_user.start
        r.end = Time.now
      end
      realization.save

      current_user.destroy

      redirect_to '/dashboard'
    end
  end

  def retrieve_task
    @start = params[:start]
    @end = params[:end]
    @user_id = params[:user_id]

    if @user_id.nil? || @user_id.empty?
      @events_constrained = Realization.where(start: @start..@end).where(user_id: get_oauth_user['result']['phid'])
    else
      @events_constrained = Realization.where(start: @start..@end).where(user_id: @user_id)
    end


    listid = []

    if @events_constrained.nil? || @events_constrained.empty?
      render :json => {"result" => {"data" => [] }}
    else
      @events_constrained.each do |event|
        id = event.task_id
        listid << id
      end
      redirect_to url_for(:controller => :conduit, :action => :search_task, :constraints => {"ids" => listid, "statuses"=> ["resolved"]})
    end
  end

  def retrieve_time
    @start = params[:start]
    @end = params[:end]
    @time = Realization.select("sum(Extract(Hour from start))*60 as start_hour, sum(Extract(Minute from start)) as start_minute, sum(Extract(Hour from realizations.end))*60 as end_hour, sum(Extract(Minute from realizations.end))as end_minute").where({start: @start..@end, user_id: get_oauth_user['result']['phid']});
    render :json => @time
  end
end
