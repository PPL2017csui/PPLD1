require 'rails_helper'

RSpec.describe DashboardController, type: :controller do

  let(:valid_attributes) {
		{
			task_id: "TASK_123",
			title: "INI TASK SEMU",
			user_id: "USER_123",
			project_id: "PROJ_123",
			start: "2017-03-30 13:00 +0700",
			end: "2017-03-30 14:00 +0700"
		}
	}

  describe "GET dashboard daily" do
    it "returns http success" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
      allow(controller).to receive(:get_oauth_user).and_return(
        {"result" => {
              "phid" => "USER_123"
            }
        }
      )
      get :daily
      expect(response).to have_http_status(200)
    end
  end

  describe "GET dashboard progress" do
    it "returns http success" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
      allow(controller).to receive(:get_oauth_user).and_return(
        {"result" => {
              "phid" => "USER_123"
            }
        }
      )

      get :progress
      expect(response).to have_http_status(200)
    end
  end

  describe "GET dashboard end_running_task" do
    it "raise DashboardError(2)" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
    	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)

      expect { get :end_running_task }.to raise_error(DashboardError)
    end

    it "ends running task successfully" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
    	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)

      current_task = User.create! ({phid: "USER_123", task_id: 1})
      get :end_running_task
      expect(response).to redirect_to('/dashboard')
    end
  end

  describe "GET dashboard update_end_time" do

    it "updates running task successfully" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
    	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)

      current_task = User.create! ({phid: "USER_123", task_id: 1, end: Time.now})
      get :update_end_time, params: {minutes: 30}
      expect(response).to redirect_to('/dashboard')
    end
  end

  describe "GET dashboard start_task" do

    it "starts running task successfully" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
    	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)

      current_task = User.create! ({phid: "USER_123", task_id: 1})
      get :end_running_task
      expect(response).to redirect_to('/dashboard')
    end
  end

  describe "GET dashboard start_event" do

    it "starts running task successfully" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
    	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)

      event = Event.create! valid_attributes
      get :start_event, params: {event_id: event.id}
      expect(response).to redirect_to('/dashboard')
    end
  end

  describe "GET #retrieve_task" do
    it "retrieve task correctly" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
      allow(controller).to receive(:get_oauth_user).and_return(
        {"result" => {
              "phid" => "USER_123"
            }
        }
      )
      schedule = Realization.create! valid_attributes
      get :retrieve_task, params: {start: "2017-03-30 00:00", end: "2017-03-30 23:59"}
      expect(response).to redirect_to (conduit_search_task_path(:constraints => {"ids" => [schedule.task_id], "statuses"=> ["resolved"]}))
    end
  end

  describe "GET #retrieve_task when list id is null" do
    it "retrieve task correctly" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
      allow(controller).to receive(:get_oauth_user).and_return(
        {"result" => {
              "phid" => "PHID-USER-qhcz6q6vyne6c6mq3yys"
            }
        }
      )
      @start = "2017-04-17 00:00"
      @end = "2017-04-24 23:59"
      get :retrieve_task, params: {start: @start, end: @end}
      expect(response.content_type).to eq("application/json")
    end
  end

  describe "GET #retrieve_task when list id is not null" do
    it "retrieve task correctly" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
      allow(controller).to receive(:get_oauth_user).and_return(
        {"result" => {
              "phid" => "USER_123"
            }
        }
      )
      schedule = Realization.create! valid_attributes
     get :retrieve_task, params: {start: "2017-03-30 00:00", end: "2017-03-30 23:59"}
     expect(response).to redirect_to (conduit_search_task_path(:constraints => {"ids" => [schedule.task_id], "statuses"=> ["resolved"]}))
    end
  end

  describe "GET #retrieve_time" do
    it "retrieve the amount of time for working on tasks" do
      allow(controller).to receive(:user_signed_in?).and_return(true)
      allow(controller).to receive(:get_oauth_user).and_return(
        {"result" => {
              "phid" => "PHID-USER-1kjhsdfkajs"
            }
        }
      )
  		@start = "2017-04-17 00:00"
    	@end = "2017-04-24 23:59"
    	get :retrieve_time, params: {start: @start, end: @end}
    	expect(response.content_type).to eq("application/json")
  	end
  end
end
