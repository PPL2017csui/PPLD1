require 'rails_helper'

RSpec.describe "custom_tasks/edit", type: :view do
  before(:each) do
    @custom_task = assign(:custom_task, CustomTask.create!(
      :title => "MyString",
      :color => "MyString"
    ))
  end

  it "renders the edit custom_task form" do
    render

    assert_select "form[action=?][method=?]", custom_task_path(@custom_task), "post" do

      assert_select "input#custom_task_title[name=?]", "custom_task[title]"

      assert_select "input#custom_task_color[name=?]", "custom_task[color]"
    end
  end
end
