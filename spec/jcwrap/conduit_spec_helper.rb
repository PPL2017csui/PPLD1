require_relative "../../lib/jcwrap/conduit.rb"
require_relative "../../lib/jcwrap/commands/maniphest.rb"
require_relative "../../lib/jcwrap/commands/user.rb"
require_relative "../../lib/jcwrap/commands/project.rb"
require_relative "../../lib/jcwrap/others/error_list.rb"
Dotenv::Railtie.load

module ConduitSpecHelper
  def conduit
    JCWrap::Conduit.new(ENV["PHHOST"],ENV["PHKEY"])
  end

  def sample_task
  	return "PHID-TASK-nuqwoe3lidhmo3d3d3r4"
  end

  def sample_user
  	return "PHID-USER-qhcz6q6vyne6c6mq3yys"
  end

  def sample_project
  	return "PHID-USER-qhcz6q6vyne6c6mq3yys"
  end
end