require "spec_helper"
require_relative "conduit_spec_helper.rb"

RSpec.configure do |c|
  c.include ConduitSpecHelper
end

RSpec.describe JCWrap::Conduit do
	it "initialize host and token correctly" do
		expect {JCWrap::Conduit.new(ENV["PHHOST"],ENV["PHKEY"]) }.not_to raise_error
	end

	it "connected to the server" do 
		expect { conduit.ping }.not_to raise_error
	end

	it "does have search method for maniphest/task" do
		expect { conduit.maniphest.search() }.not_to raise_error
	end	

	it "does have search method for project" do
		expect { conduit.project.search() }.not_to raise_error
	end

	it "does have search method for user" do
		expect { conduit.user.search() }.not_to raise_error
	end

	it "does have find method for maniphest/task" do
		expect { conduit.maniphest.find([sample_task]) }.not_to raise_error
	end	

	it "does have find method for project" do
		expect { conduit.maniphest.find([sample_project]) }.not_to raise_error
	end	

	it "does have find method for user" do
		expect { conduit.maniphest.find([sample_user]) }.not_to raise_error
	end	
end