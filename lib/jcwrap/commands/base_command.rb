require_relative '../others/error_list.rb'

module JCWrap
	# BaseCommand is the base of all the commands class
	# The program takes Conduit as instance variable, so it can call the methods from Conduit
	# Do not use this class to call the methods, use the Conduit class instead
	# Author:: Jadvalee

	class BaseCommand
		attr_reader :conduit

		def initialize(conduit)
			@conduit = conduit
		end

		# search wraps all the variables to params, which is parameter for the request, then send it to the server
		# params:
		# all the parameter needs from the Conduit API, please read the Conduit API docs
		# for more info.
		def search(method_name: nil, query_key: 'all', constraints: {}, attachments: {}, 
					order: nil, before: nil, after: nil, limit: 100)
			if method_name == nil
				raise NilValueError, 'method_name cant be nil!'
			end

			params = {
				'queryKey' => query_key,
				'constraints' => constraints,
				'attachments' => attachments,
				'order' => order,
				'before' => before,
				'after' => after,
				'limit' => limit
			}

			response = @conduit.send_request(method_name, params: params)
			return response
		end

		# get Phabricathor Object by PHID using phid.query method as default
		# params:
		# +phid+:: phabricator object's id
		# please read the phid.query method on the Conduit API for more info.
		def get_phids(phids, method_name: 'phid.query')
			if phids == nil
				raise NilValueError, "phid cant be nil!"
			end

			params = {
				'phids' => phids
			}

			response = @conduit.send_request(method_name, params: params)
			return response
		end
	end
end