
module JCWrap
	# NilValueError class
	# handling nil value for any required parameter on JCWrap program
	#
	# Author:: Jadvalee

	class NilValueError < StandardError; end

	# ResponseError
	# handling feedback error code from the host
	#
	# Author:: Jadvalee
	class ResponseError < StandardError; end
end