require 'rails_helper'

RSpec.describe AuthController, type: :controller do
	describe "GET #login" do 
			
		it "render login_success when session is valid" do
			allow(controller).to receive(:user_signed_in?).and_return(true);
			get :login
			
			expect(response).to render_template("login_success")
		end

		it "render login_success when oauth callback with code" do
			allow(controller).to receive(:get_oauth_token).with("dummy_code") { "dummy_token" }
			get :login, params: { code: "dummy_code" }
			
			expect(response).to render_template("login_success")
			expect(session[:access_token]).to eq("dummy_token")
		end

		it "render login when no session and no callback with code" do
			get :login

			expect(response).to render_template("login")
		end
	end

	describe "GET #logout" do
		it "destroy session and redirect to auth#login" do
			get :logout

			expect(response).to redirect_to("/login")
			expect(session[:access_token]).to be_nil
		end
	end
end
