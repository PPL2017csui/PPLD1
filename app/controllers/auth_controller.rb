class AuthController < ApplicationController
	skip_before_action :authenticate!, only: [:login]

	def login 
		unless user_signed_in?
			if params[:code]
				session[:access_token] = get_oauth_token(params[:code])
				render action: 'login_success', :layout => false
			else
				render action: 'login', :layout => false
			end
		else
			render action: 'login_success', :layout => false
		end
	end

	def logout
		reset_session
		redirect_to '/login'
	end
end
