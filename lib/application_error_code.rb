# All the Jadvalee's Error codes
# code format XXXX, where X 1..9

# AuthenticationError
# Handling feedback error code from oauth token request
#
# Author:: Jadvalee
class AuthenticationError < StandardError
	def self.code
		1001
	end
end

class SettingsError < StandardError
	def code(code)
		"5#{code.to_s.rjust(3,'0')}"
	end

	def message(code)
		["Time input is empty!", "Please fill the fields and try again"][code-1]
	end

end

# EventsError
# Handling feedback error code from EventsController
#
# Author:: Jadvalee
class EventsError < StandardError

	def self.code(code)
		"2#{code.to_s.rjust(3,'0')}"
	end

	def self.message(code)
		["Overlaping task detected on your/tagged user(s) schedule, or your task is not on the focus time interval", "Missing value on the required field(s)", "Not authorized action"][code-1]
	end
end

# DashboardError
# Handling feedback error code from DashboardController
#
# Author:: Jadvalee
class DashboardError < StandardError

	def self.code(code)
		"3#{code.to_s.rjust(3,'0')}"
	end

	def self.message(code)
		["Running task detected! please stop the current task to start","No running task detected!"][code-1]
	end
end

# CustomTaskError
# Handling feedback error code from CustomTasksController
#
# Author:: Jadvalee
class CustomTaskError < StandardError

	def self.code(code)
		"4#{code.to_s.rjust(3,'0')}"
	end

	def self.message(code)
		["Title required!"][code-1]
	end
end
