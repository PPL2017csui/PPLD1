class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :title
      t.datetime :start
      t.datetime :end
      t.string :task_id
      t.string :user_id
      t.string :project_id
      t.string :color
      t.string :desc, :default => "No Description"
      t.text :tag, array: true, :default => []

      t.timestamps
    end
  end
end
