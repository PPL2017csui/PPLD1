class CreateRealizations < ActiveRecord::Migration[5.0]
  def change
    create_table :realizations do |t|
      t.string :title
      t.datetime :start
      t.datetime :end
      t.string :task_id
      t.string :user_id
      t.string :project_id
      t.string :color
      t.string :desc
      t.string :tag
      
      t.timestamps
    end
  end
end
