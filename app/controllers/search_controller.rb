class SearchController < ApplicationController

	def search
		render action: 'search'
	end

end
