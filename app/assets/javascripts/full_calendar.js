var initialize_calendar;

initialize_business_hour = function () {
 var result = '[';

 $.getJSON('/settings/retrieve_focus',function(data) {
  var arr = $.map(data, function(el) { return el });
     for(i = 0; i < arr.length; i++) {
         var start = (arr[i]['focus_time_start']);
         var end = (arr[i]['focus_time_end']);
         var dow = (arr[i]['day_id']);
         result += ('{ "start": "' + start + '", "end": "' + end + '", "dow": ['+ dow + ']}');
         if(i < arr.length-1)
          result += ',';
  }
  result += ']';
  result = JSON.parse(result);
  initialize_calendar(result);
 });

}

initialize_calendar = function (result) {
	$('.main-calendar, #realization-calendar, #planned-calendar').each(function () {
		function ini_events(ele) {
      		ele.each(function () {

	        	var eventObject = {
	          		title: $.trim($.trim($(this).text()).substr(0,$.trim($(this).text()).length-1)) // use the element's text as the event title
	       		};

      			// store the Event Object in the DOM element so we can get to it later
        		$(this).data('eventObject', eventObject);

       			// make the event draggable using jQuery UI
        		$(this).draggable({
         		 zIndex: 1070,
          		 revert: true, // will cause the event to go back to its
         		 revertDuration: 0  //  original position after the drag
        		});

     		});
  		}

  		ini_events($('#external-events div.external-event'));

  		/* ADDING EVENTS */
	    var currColor = "#135959"; //Red by default

	    $("#color-chooser > li > a").click(function (e) {
	    	e.preventDefault();
	    	//Save color
	    	currColor = $(this).css("color");
	    	//Add color effect to button
	    	$('.customtask-button').css({"background-color": currColor, "border-color": currColor});

	    });

		var calendar = $(this);
		function get_calendar_height() {
			return $(window).height();
		}

		$(window).resize(function () {
			$('.main-calendar').fullCalendar('option', 'height', get_calendar_height());
		});


		$('#realization-calendar').fullCalendar({
	  timeFormat: 'H:mm',
      header: false,
      allDaySlot: false,
      height: get_calendar_height(),
      droppable: false,
      editable: false,
      defaultView: 'basicDay',

      events: '/realizations.json',
      viewRender: function(view, element) {
            element.find('.fc-day-header').html('');
        },

		loading: function( isLoading, view) {
			if(isLoading)
				$('.loading-screen').show();
			else
				$('.loading-screen').hide();
		}
    });

$('#planned-calendar').fullCalendar({
      header: false,
      allDaySlot: false,

      timeFormat: 'H:mm',
      height: get_calendar_height(),
      defaultView: 'basicDay',
      editable: false,
      droppable: false, // this allows things to be dropped onto the calendar !!!
	  events: '/events.json',
      viewRender: function(view, element) {
            element.find('.fc-day-header').html('');
        },

		loading: function( isLoading, view) {
			if(isLoading)
				$('.loading-screen').show();
			else
				$('.loading-screen').hide();
		},

      eventRender: function(event, element, view) {

			element.find('.fc-title').append("<br><a href='/dashboard/start_event/"+ event.id +"' class='plan-btn btn bg-olive pull-right'> Start </a>");
        }
    });

		$('.main-calendar').fullCalendar({
			header: {
				left: 'today',
				center: 'prev,title,next',
				right: 'agendaWeek,agendaDay'
			},
			buttonText: {
				today: 'Today',
				week: 'Weekly',
				day: 'Daily'
			},

			eventRender: function(event, element) {
				tags = [""];
				if (!(event.tag === undefined || event.tag === null))
					tags = event.tag;

				tag_text = "";
				for(var i = 0; i < tags.length; i++) {
					tag_text += "@" + tags[i];
					if (i != tags.length-1)
						tag_text += ", ";

				}
				element.find('.fc-title').append("<br/>" + event.description + "<br/>" + tag_text);
			},

			allDaySlot: false,
			defaultView: 'agendaDay',
			editable: true,
			droppable: true,
			selectable: true,
			selectHelper: true,
			eventOverlap: false,
      slotDuration: '00:10:00',
			eventLimit: true,
			timeFormat: 'H:mm',
			businessHours: result,
			loading: function( isLoading, view) {
				if(isLoading)
					$('.loading-screen').show();
				else
					$('.loading-screen').hide();
			},
			events: '/events.json',

			drop: function (date, allDay) { // this function is called when something is dropped

		        var originalEventObject = $(this).data('eventObject');

		        // we need to copy it, so that multiple events don't have a reference to the same object
		        var copiedEventObject = $.extend({}, originalEventObject);

		        copiedEventObject.backgroundColor = $(this).css("background-color");
		        copiedEventObject.borderColor = $(this).css("border-color");

		        var start = date;
		        var end = moment(date).add(2, 'hours');

		        event_data = {
					event: {
						task_id: "CUSTOM",
						title: "(CUSTOM): "+copiedEventObject.title,
						desc: "No Description",
						start: moment(start).format('YYYY-MM-DD HH:mm'),
						end: moment(end).format('YYYY-MM-DD HH:mm')
					}
				};


				$.ajax({
					url: '/events',
					data: event_data,
					type: 'POST',
					error: function(xhr, status, error) {
				        if(error == "Not Acceptable") {
				            var json_response = JSON.parse(xhr.responseText);
				            $('#report-error-code').html("(" + json_response['error_code'] + ")");
				            $('#report-error-message').html(json_response['error_message']);
				            $('.report-message').slideDown('fast');
				        } else {
				            $('#report-error-code').html("(1000)");
				            $('#report-error-message').html("Unknown error! please try again");
				            $('.report-message').slideUp('fast');
				        }
					},
					beforeSend: function(xhr, settings) {
        				$('.loading-screen').show();
        			}

				}).always(function(data) {
					$('.loading-screen').hide();
				});
	     	},

			select: function (start, end) {
				$.getScript('/events/new', function () {
					$('#event_date_range').val(moment(start).format("MM/DD/YYYY HH:mm") + ' - ' + moment(end).format("MM/DD/YYYY HH:mm"));
					date_range_picker();
					$('.start_hidden').val(moment(start).format('YYYY-MM-DD HH:mm'));
					$('.end_hidden').val(moment(end).format('YYYY-MM-DD HH:mm'));
				});
			},
			eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
				event_data = {
					event: {
						id: event.id,
						start: event.start.format(),
						end: event.end.format()
					}
				};
				$.ajax({
					url: event.update_url,
					data: event_data,
					type: 'PATCH',
					error: function(xhr, status, error) {
				        if(error == "Not Acceptable") {
				            var json_response = JSON.parse(xhr.responseText);
				            $('#report-error-code').html("(" + json_response['error_code'] + ")");
				            $('#report-error-message').html(json_response['error_message']);
				            $('.report-message').slideDown('fast');
				        } else {
				            $('#report-error-code').html("(1000)");
				            $('#report-error-message').html("Unknown error! please try again");
				            $('.report-message').slideUp('fast');
				        }
				        revertFunc();
					},
					beforeSend: function(xhr, settings) {
        				$('.loading-screen').show();
        			}

				}).always(function(data) {
					$('.loading-screen').hide();
				});
			},
			eventDrop: function (event, delta, revertFunc) {

				event_data = {
					event: {
						id: event.id,
						start: event.start.format(),
						end: event.end.format()
					}
				};
				$.ajax({
					url: event.update_url,
					data: event_data,
					type: 'PATCH',
					error: function(xhr, status, error) {
				        if(error == "Not Acceptable") {
				            var json_response = JSON.parse(xhr.responseText);
				            $('#report-error-code').html("(" + json_response['error_code'] + ")");
				            $('#report-error-message').html(json_response['error_message']);
				            $('.report-message').slideDown('fast');
				        } else {
				            $('#report-error-code').html("(1000)");
				            $('#report-error-message').html("Unknown error! please try again");
				            $('.report-message').slideUp('fast');
				        }
				        revertFunc();
					},
					beforeSend: function(xhr, settings) {
        				$('.loading-screen').show();
        			}

				}).always(function(data) {
					$('.loading-screen').hide();
				});
			},

			eventClick: function (event, jsEvent, view) {
				$('.loading-screen').show();
				$.getScript(event.edit_url, function () {
					$('#event_date_range').val(moment(event.start).format("MM/DD/YYYY HH:mm") + ' - ' + moment(event.end).format("MM/DD/YYYY HH:mm"))
					date_range_picker();
					$('.start_hidden').val(moment(event.start).format('YYYY-MM-DD HH:mm'));
					$('.end_hidden').val(moment(event.end).format('YYYY-MM-DD HH:mm'));
				}).done(function() {
					$('.loading-screen').hide();
				});
			}
		});
	})
};
$(document).on('ready', initialize_business_hour);
