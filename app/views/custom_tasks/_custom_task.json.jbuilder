json.extract! custom_task, :id, :title, :color, :created_at, :updated_at
json.url custom_task_url(custom_task, format: :json)
