require_relative 'base_command.rb'
module JCWrap
	class User < BaseCommand

		def search(query_key: 'all', constraints: {}, attachments: {},
				order: nil, before: nil, after: nil, limit: 100)
			super(method_name: 'user.search', query_key: query_key, constraints: constraints,
				attachments: attachments, order: order, before: before, after: after, limit: limit)

		end

		def find(phids) 
			response = get_phids(phids, method_name: 'user.query')
		end
	end
end