

class SettingsController < ApplicationController
	before_action :set_default_response_format, only: :retrieve_focus
	before_action :authenticate!, :business_hour_check

	def setting
		@settings = Setting.where(user_id: get_oauth_user['result']['phid'])

		@settings_error = SettingsError.new
		render action: 'setting'
	end

	def set_focus
		@setting = Setting.where({user_id: params[:user_id], day_id: params[:day_id]})
		if @setting.count > 0
			@setting.update(params.permit(:user_id, :day_id, :focus_time_start, :focus_time_end))
		else
			Setting.create(params.permit(:user_id, :day_id, :focus_time_start, :focus_time_end))
		end
	end

	def retrieve_focus
  		@setting = Setting.where(user_id: get_oauth_user['result']['phid'])
  		render :json => @setting
	end

	def set_default_response_format
		request.format = :json
	end
end
