class ScheduleController < ApplicationController
	before_action :set_default_response_format, only: :daily
	before_action :authenticate!
	before_action :business_hour_check, only: :index

	def daily
		@schedule = Schedule.where({date: params[:date]})
		render :json => @schedule
	end

	def save_schedule
		@schedule = Schedule.where({user_id: params[:user_id], task_id: params[:task_id], project_id: params[:project_id], date: params[:date]})
		if @schedule.count > 0
			@schedule.update(params.require(:schedule).permit(:user_id, :task_id, :project_id, :date, :time_start,:time_end, :created_at, :updated_at))

		else
		Schedule.create(params.require(:schedule).permit(:user_id, :task_id, :project_id, :date, :time_start,:time_end, :created_at, :updated_at))
		end
	end

	def index
		render action: 'index'
	end

	def set_default_response_format
	  request.format = :json
	end
end
