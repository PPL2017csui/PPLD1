class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :phid
      t.string :task_id
      t.integer :event_id
      t.datetime :start
      t.datetime :end

      t.timestamps
    end
  end
end
