require 'rails_helper'

RSpec.describe SettingsController, type: :controller do
	let(:valid_attributes) {
    	{
			user_id: "PHID-USER-qhcz6q6vyne6c6mq3yys",
			day_id: "1",
			focus_time_start: "08.00",
			focus_time_end: "16.00"
		}
  	}

	# describe "POST #set_focus" do
	# 	it "replacing the old setting with the new setting" do
	# 		expect {
	# 			post :set_focus, params: {setting: valid_attributes}
	# 		}.to change(Setting.where({day_id: "1"}), :count).to(0)
	# 	end
	# end

	describe "GET #retrieve_focus" do
		it "responds to json by default" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
			allow(controller).to receive(:get_oauth_user).and_return(
			{
				"result" => {
			      "phid" => "PHID-USER-qhcz6q6vyne6c6mq3yys"
			    }
			}
			)
			get :retrieve_focus
			expect(response.content_type).to eq("application/json");
		end
	end

	describe "GET #setting" do
		it "returns http success" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
			allow(controller).to receive(:get_oauth_user).and_return(
			{
				"result" => {
			      "phid" => "PHID-USER-qhcz6q6vyne6c6mq3yys"
			    }
			}
			)
			get :setting
			expect(response).to have_http_status(:success)
		end
	end
end
