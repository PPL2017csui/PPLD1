Rails.application.routes.draw do
	root to: 'auth#login'
	
	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  	get '/conduit/search_task', to: 'conduit#search_task'
  	get '/conduit/search_user', to: 'conduit#search_user'
  	get '/conduit/search_project', to: 'conduit#search_project'
	get '/conduit/get_all_events', to: 'conduit#get_all_events'
	get '/schedule', to: 'schedule#index'
  	get '/schedule/daily', to: 'schedule#daily'
	post '/schedule/save_schedule', to: 'schedule#save_schedule'
	get '/dashboard', to: 'dashboard#daily'
	get '/events/realization/:id', to: 'events#set_realization'
	get '/dashboard/progress', to: 'dashboard#progress'
	get '/dashboard/retrieve_task', to: 'dashboard#retrieve_task'
	get '/dashboard/retrieve_time', to: 'dashboard#retrieve_time'
	get '/search', to: 'search#search'
	get '/login', to: 'auth#login', as: :login
	get '/logout', to: 'auth#logout', as: :logout
	get '/dashboard/start_task/:task_id', to: 'dashboard#start_task'
	get '/dashboard/start_event/:event_id', to: 'dashboard#start_event'
	get '/dashboard/end_running_task', to: 'dashboard#end_running_task'
	post '/dashboard/update_end_time', to: 'dashboard#update_end_time'
	post '/settings/set_focus', to: 'settings#set_focus'
	get '/settings/retrieve_focus', to: 'settings#retrieve_focus'
	get '/settings', to: 'settings#setting'

	resources :events
	resources :settings
	resources :realizations
    resources :custom_tasks
    resources :customs
end