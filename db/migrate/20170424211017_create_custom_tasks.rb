class CreateCustomTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :custom_tasks do |t|
      t.string :title
      t.string :color

      t.timestamps
    end
  end
end
