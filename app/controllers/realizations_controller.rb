class RealizationsController < ApplicationController
	before_action :set_event, only: [:show, :edit, :update, :destroy]
	before_action :authenticate!

	def index
		@realizations = Realization.where(start: params[:start]..params[:end]).where(user_id: get_oauth_user['result']['phid'])
	end

	def create
		@realization = Realization.new(realization_params)
	end

	def new
		@event = Event.new
	end

	private
	    def set_realization
	      @realization = Realization.find(params[:id])
	    end

	    def realization_params
	      params.require(:realization).permit(:title, :date_range, :start, :end, :color, :task_id, :desc, :tag, :user_id, :project_id, :manual_date)
	    end
end
