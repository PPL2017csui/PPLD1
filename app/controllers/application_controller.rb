require 'net/http'
require 'uri'
require 'json'
require 'application_error_code'
require 'jcwrap/conduit'

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :user_signed_in?, :oauth_url, :get_oauth_user, :print_error, :get_user_running_task
  before_action :create_conduit_instance, :authenticate!

  def authenticate!
  	redirect_to "/login" unless user_signed_in?
  end

  def business_hour_check
    if(Setting.where(user_id: get_oauth_user['result']['phid']).count() != 7)
      Setting.find_or_create_by({ user_id: get_oauth_user['result']['phid'] , day_id: 7})
      Setting.find_or_create_by({ user_id: get_oauth_user['result']['phid'] , day_id: 1})
      Setting.find_or_create_by({ user_id: get_oauth_user['result']['phid'] , day_id: 2})
      Setting.find_or_create_by({ user_id: get_oauth_user['result']['phid'] , day_id: 3})
      Setting.find_or_create_by({ user_id: get_oauth_user['result']['phid'] , day_id: 4})
      Setting.find_or_create_by({ user_id: get_oauth_user['result']['phid'] , day_id: 5})
      Setting.find_or_create_by({ user_id: get_oauth_user['result']['phid'] , day_id: 6})
    end
  end

  def user_signed_in?
  	unless session[:access_token].nil?
  		return get_oauth_user['error'].nil?
  	else
  		return false
  	end
  end

  def get_user_running_task
    current_user_task = User.find_or_create_by(phid: get_oauth_user['result']['phid'])
		running_task = nil

    if current_user_task.event_id != nil
      event = Event.find(current_user_task.event_id)
      running_task = Event.new
      running_task.title = event.title
			running_task.start = current_user_task.start
      running_task.desc = event.desc
			running_task.end = current_user_task.end
    elsif current_user_task.task_id != nil
			running_task = Event.new
			task = @conduit.maniphest.search(limit: 1, constraints: { "ids" => [current_user_task.task_id.to_i]})
			running_task.title = "(#{current_user_task.task_id}): #{task['result']['data'][0]['fields']['name']}"
			running_task.start = current_user_task.start
      running_task.desc = ""
			running_task.end = current_user_task.end
		end
		return running_task
	end

  def create_conduit_instance
      @conduit = JCWrap::Conduit.new(ENV["PHHOST"], ENV["PHKEY"])
  end

	def get_oauth_token(code)
		params = {
			grant_type: 'authorization_code',
			code: code,
			client_id: ENV['OAUTH_CLIENT_ID'],
			client_secret: ENV['OAUTH_CLIENT_SECRET'],
			redirect_uri: ENV['OAUTH_CLIENT_REDIRECT_URI']
		}

		request = Net::HTTP::Post.new('/oauthserver/token/?' + URI.encode_www_form(params))

		return send_request(request)['access_token'];
  end

  def get_oauth_user
		request = Net::HTTP::Get.new('/api/user.whoami?' + "access_token=#{session[:access_token]}")

		return send_request(request)
  end

  def send_request(request)
  	http = Net::HTTP.new(oauth_server_uri.host, oauth_server_uri.port)
  	http.use_ssl = (oauth_server_uri.scheme == 'https')
  	begin
  		response = http.request(request)
  	rescue Timeout::Error
  		raise print_error(522, "host #{@host} timed out")
  	end

  	json_response =  JSON.parse(response.body)

  	unless json_response['error'].nil?
  		raise AuthenticationError, print_error(AuthenticationError.code, json_response['error_description'])
  	end

  	return json_response
  end

  def oauth_url
  	return "#{ENV['PHHOST']}/oauthserver/auth/?response_type=code&client_id=#{ENV['OAUTH_CLIENT_ID']}"
  end

  def oauth_server_uri
  	URI(ENV['PHHOST'])
  end

  def print_error(code, message)
  	return "(#{code}): #{message}"
  end
end
