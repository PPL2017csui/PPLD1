![Build Status](https://gitlab.com/PPL2017csui/PPLD1/badges/feature/create-schedule_%23140674885/build.svg)
[![codecov](https://codecov.io/gl/PPL2017csui/PPLD1/branch/feature%2Fcreate-schedule_%23140674885/graph/badge.svg)](https://codecov.io/gl/PPL2017csui/PPLD1)
=======
# JADVALEE
#### Tech
---

* [Ruby](https://rubyinstaller.org/downloads/) - version 2.3.1
* [Rails](http://railsinstaller.org/en) 
* [Node.js](https://nodejs.org/en/download/)

#### How to install Jadvalee?
---

#### Ubuntu
##### Setup Git and Repository

1. Install GIT on your local

    ```sh
     sudo apt-get install git
    ```

2. Configure GIT
    ```sh
    git config --global user.name "[your-username]"
    git config --global user.email "[your-gitlab-email]"
    ```
3. Clone Repository

    ```sh
    git clone https://gitlab.com/PPL2017csui/PPLD1.git
    cd PPLD1
    ```

##### Install RVM and Ruby

1. Install Curl

    ```sh
    sudo apt-get install curl
    ```
2. Install RVM

    ```sh
    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
    \curl -sSL https://get.rvm.io | bash -s stable
    source ~/.rvm/scripts/rvm
    echo "source ~/.rvm/scripts/rvm" >> .bashrc
    ```
3. Install Ruby

    ```sh
    rvm install 2.3.1
    ```
##### Setup and Run Jadvalee
1. Install Dependencies
    
    ```sh
    sudo apt-get -qq update
    sudo apt-get install lcov
    ```

2. Install PostgreSQL

    ```sh
    sudo apt-get install postgresql postgresql-contrib
    ```

3. Install Node.js

    ```sh
    sudo apt-get install curl
    \curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
    sudo apt-get install -y nodejs
    ```

4. Install Bundler

    ```sh
    gem install bundler
    ```

5. Install Gems

    ```sh
    bundle install
    ```

6. Install Node Packages

    ```sh
    npm install
    ```

7. Copy env.sample to .Env File

    ```sh
    cp env.sample .env 
    ```

8. Database Migration

    ```sh
    rails db:create
    rails db:migrate 
    ```

9. Run Jadvalee

    ```sh
    rails server
    ```

10. Access via browser at `http://localhost:3000`

#### Windows
##### Setup Git and Repository

1. Install GIT
    Download from https://git-scm.com/download/win
    
2. Add GIT on Environment Variables
    Add GIT path to your environment variables

3. Configure GIT
    ```sh
    git config --global user.name "[your-username]"
    git config --global user.email "[your-gitlab-email]"
    ```

4. Clone Repository
    ```sh
    git clone https://gitlab.com/PPL2017csui/PPLD1.git
    cd PPLD1
    ```

##### Install Ruby
1. Download Ruby v2.3.1
    Download Ruby v2.3.1 from http://rubyinstaller.org/downloads/archives

2. Add Ruby to Environment Variables
    Add Ruby path to your environment variables

##### Setup and Run Jadvalee
1. Download PostgreSQL
    Download the latest version from https://www.postgresql.org/download/windows/

2. Download Node.js
    Download from https://nodejs.org/en/download

3. Install Bundler
    ```sh
    gem install bundler
    ```

4. Install Gems
    ```sh
    bundle install
    ```
5. Install Node Packages
    ```sh
    npm install
    ```
    
6. Copy env.sample to .Env File
    Copy env.sample in root to .env file then make changes if needed

7. Database Migration
    ```sh
    rails db:create
    rails db:migrate 
    ```
8. Run Jadvalee
    ```sh
    rails server
    ```

9. Access via browser at `http://localhost:3000`