class CreateSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :settings do |t|
      t.string :user_id
      t.string :day_id
      t.string :focus_time_start, :default => "09:00"
      t.string :focus_time_end, :default => "17:00"

      t.timestamps
    end
  end
end
