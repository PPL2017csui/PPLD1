require 'rails_helper'

RSpec.describe "custom_tasks/new", type: :view do
  before(:each) do
    assign(:custom_task, CustomTask.new(
      :title => "MyString",
      :color => "MyString"
    ))
  end

  it "renders new custom_task form" do
    render

    assert_select "form[action=?][method=?]", custom_tasks_path, "post" do

      assert_select "input#custom_task_title[name=?]", "custom_task[title]"

      assert_select "input#custom_task_color[name=?]", "custom_task[color]"
    end
  end
end
