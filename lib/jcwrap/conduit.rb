require 'uri'
require 'net/http'
require 'json'

require_relative './commands/maniphest.rb'
require_relative './commands/project.rb'
require_relative './commands/user.rb'
require_relative './others/error_list.rb'

module JCWrap
	# Conduit is the main for the Conduit API Wrapper.
	# The program takes host and token for requesting through
	# the Conduit API methods for Jadvalee needs.
	#
	# Author:: Jadvalee
	# 
	# use example : JCWrap::Conduit(host, token).maniphest.search()
	# resulting list of task from the server

	class Conduit
		attr_reader :host, :token, :maniphest, :project, :user
		
		# Conduit class Constructor
		# params:
		# +host+:: the host for the phabricator example: http://secure.phabricator.com
		# +token+:: api/client api to access the Conduit API
		def initialize(host, token)
			@host = host
			@token = token
			@maniphest = Maniphest.new(self)
			@project = Project.new(self)
			@user = User.new(self)
			@uri = URI(@host)

			if @uri.host == nil || @uri.port == nil
				raise NilValueError, "host value is invalid! valid example: (http://example.com)"
			end

			if token == nil
				raise NilValueError, "token value cant be nil!"
			end
		end

		# Send request to retrieve data from the server
		# params:
		# +method_name+:: the method will be called for the request. example: http://secure.phabricator.com/api/method_name
		# +params+:: the parameter for the request (optional)
		# +output+:: the output from the request, default: json (optional)
		def send_request(method_name, params: {}, output: 'json')

			http = Net::HTTP.new(@uri.host, @uri.port)
			http.use_ssl = (@uri.scheme === 'https')

			params['__conduit__'] = { 
				'token' => @token
			}

			request = Net::HTTP::Get.new("/api/#{method_name}")
			request.set_form_data({ 
				'__conduit__' => 1, 
				'output' => output, 
				'params' => params.to_json.to_s 
			})
			
			begin
				response = http.request(request)
			rescue Timeout::Error
				raise "host #{@host} timed out"
			end

			response_json = JSON.parse(response.body)

			if response_json['error_code'] != nil
				raise ResponseError, "failed to request data from the server! (#{response_json['error_code']}): #{response_json['error_info']}" 
			end
			
			return response_json
		end

		def ping
			return send_request('conduit.ping')
		end
	end
end