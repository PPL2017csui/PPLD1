require 'json'

class ConduitController < ApplicationController

	def is_numeric? string
		true if Integer(string) rescue false
	end

	def search_task
		constraints = {}
		constraints = params[:constraints] unless params[:constraints].nil?

		unless params[:context].nil?
			context = params[:context]

			if context[0] == 'T' && is_numeric?( context[1..context.length-1] )
				constraints = {"ids" => [context[1..context.length-1].to_i]};
			else
				constraints = {"fulltext" => context.to_s}
			end

			@tasks = @conduit.maniphest.search(limit: 10, constraints: constraints, attachments: { "projects" => true })
		else
			constraints[:ids] = constraints[:ids].map(&:to_i) unless constraints[:ids].nil?
			@tasks = @conduit.maniphest.search(limit: 10, constraints: constraints, attachments: { "projects" => true })
		end
		render :json => @tasks
	end

	def search_user
		constraints = {}
		constraints = params[:constraints] unless params[:constraints].nil?
		unless params[:name_like].nil?
			constraints[:nameLike] = params[:name_like]
			@users = @conduit.user.search(limit: 10, constraints: constraints)
			render :json => @users
		else
			constraints[:phids] = params[:members_id]
			@phid = params[:phid]
			@users = @conduit.user.search(constraints: constraints)
			render :json => {:phid => @phid, :users => @users}
		end
	end

	def search_project
		constraints = {}
		constraints = params[:constraints] unless params[:constraints].nil?
		constraints[:name] = params[:name_like]

		projects = @conduit.project.search(limit: 10, constraints: constraints, attachments: {"members" => true, "watchers" => true})

		data = JSON.parse(projects.to_json)
		array = []

		data['result']['data'].each_with_index do |section, i|
			array << {"phid" => "#{section['phid']}", "project name" => "#{section['fields']['name']}", "members" => [], "watchers" => []}
			section['attachments'].each do |innersection|
				innersection.each do |element|
					if element.class == Hash
						element.each do |key, value|
							value.each do |e|
								e.each do |k, v|
									if key == "members"
										array[i]['members'] << "#{v}"
									elsif key == "watchers"
										array[i]['watchers'] << "#{v}"
									end
								end
							end
						end
					end
				end
			end 
		end
		render :json => array
	end

	def get_all_events
		users_id = params[:users_id]
		users_info = @conduit.user.find(users_id)
		users = []
		usernames = []

		users_info['result'].each_with_index do |section, i|
			users.push(section)
			usernames.push(section['userName'])
		end unless users_info.nil?

		events = Event.where("user_id IN (?) OR tag && ARRAY[?]::text[]", users_id, usernames)
		array = []
		user = []
		tagged_user_info = []
		tagged_users = []

		events.each do |event|
			title = event.title
			year = event.start.year
			month = event.start.month
			day = event.start.day
			start_hour = event.start.hour
			start_minute = event.start.min
			end_hour = event.end.hour
			end_minute = event.end.min
			user_id = event.user_id
			user = users.select {|user| user["phid"] == user_id}
			tag = event.tag

			unless user.empty?
				array << {"name" => "#{user[0]['realName']}", "event" => title, "year" => year, "month" => month, "day" => day, "startHour" => start_hour, "startMinute" => start_minute, "endHour" => end_hour, "endMinute" => end_minute}
			end
			
			unless tag.empty?
				tag.each_with_index do |u, i|

					if users.any? {|us| us["userName"] == "#{u}"}
						tagged_user_info = users.select {|user| user["userName"] == "#{u}"}
						array << {"name" => "#{tagged_user_info[0]['realName']}", "event" => title, "year" => year, "month" => month, "day" => day, "startHour" => start_hour, "startMinute" => start_minute, "endHour" => end_hour, "endMinute" => end_minute}
						tagged_users << tagged_user_info
					end
				end
			end

		end unless events.nil?

		render :json => array
	end
end