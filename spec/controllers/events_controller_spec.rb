require 'rails_helper'

RSpec.describe EventsController, type: :controller do
	before :each do
		request.headers["accept"] = 'application/javascript'
	end

	let(:valid_attributes) {
    	{
    		task_id: "TASK_123",
    		title: "INI TASK SEMU",
    		user_id: "USER_123",
    		project_id: "PROJ_123",
    		start: "2017-03-30 13:00 +0700",
    		end: "2017-03-30 14:00 +0700"
    	}
  	}

	describe "GET #index" do
		it "events retrieved correctly" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
	      	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)
			event = Event.create! valid_attributes
			get :index, params: {start: "2017-03-30 00:00", end: "2017-03-30 23:59", format: :json}
			expect(response.content_type).to eq("application/json")
			expect(assigns(:events)).to eq([event])
		end
	end

	describe "GET #show" do
		it "assigns the requested event as @event" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
	      	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)
			event = Event.create! valid_attributes
			get :show, params: {id: event.to_param, format: :json}
			expect(assigns(:event)).to eq(event)
		end
	end

	describe "GET #new" do
		it "assigns a new event as @event" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
	      	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)
			get :new, params: {}, xhr: true
			expect(assigns(:event)).to be_a_new(Event)
		end
	end

	describe "GET #edit" do
		it "assigns the requested event as @event" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
	      	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)
			event = Event.create! valid_attributes
			get :edit, params: {id: event.to_param}, xhr: true
			expect(assigns(:event)).to eq(event)
		end
	end

	describe "POST #create" do
		context "with valid params" do
			it "creates a new Event" do
				allow(controller).to receive(:user_signed_in?).and_return(true)
		      	allow(controller).to receive(:get_oauth_user).and_return(
					{"result" => {
					      "phid" => "USER_123"
					    }
					}
				)
        		expect {
          			post :create, params: {event: valid_attributes}, xhr: true
        		}.to change(Event, :count).by(1)
      		end

      		it "assigns a newly created event as @event" do
				allow(controller).to receive(:user_signed_in?).and_return(true)
		      	allow(controller).to receive(:get_oauth_user).and_return(
					{"result" => {
					      "phid" => "USER_123"
					    }
					}
				)
        		post :create, params: {event: valid_attributes}, xhr: true
		        expect(assigns(:event)).to be_a(Event)
		        expect(assigns(:event)).to be_persisted
      		end

      		it "returns success code (200)" do
				allow(controller).to receive(:user_signed_in?).and_return(true)
		      	allow(controller).to receive(:get_oauth_user).and_return(
					{"result" => {
					      "phid" => "USER_123"
					    }
					}
				)
        		post :create, params: {event: valid_attributes}, xhr: true
        		expect(response).to have_http_status(200)
     		end
		end
	end

	describe "PUT #update" do
		context "with valid params" do
			let(:new_attributes) {
		    	{
		    		task_id: "TASK_123",
		    		title: "INI TASK SEMU HEHO",
		    		user_id: "USER_123",
		    		project_id: "PROJ_123",
		    		start: "2017-03-30 12:00 +0700",
		    		end: "2017-03-30 15:00 +0700"
		    	}
	     	}

	     	it "updates the requested event" do
				allow(controller).to receive(:user_signed_in?).and_return(true)
		      	allow(controller).to receive(:get_oauth_user).and_return(
					{"result" => {
					      "phid" => "USER_123"
					    }
					}
				)
	     		event = Event.create! valid_attributes
	     		put :update, params: {id: event.to_param, event: new_attributes}, xhr: true
	     		event.reload
	     		expect(event.title).to eq(new_attributes[:title])
	     		expect(event.start).to eq(new_attributes[:start])
	     		expect(event.end).to eq(new_attributes[:end])
	     	end

	     	it "assigns the requested event as @event" do
				allow(controller).to receive(:user_signed_in?).and_return(true)
		      	allow(controller).to receive(:get_oauth_user).and_return(
					{"result" => {
					      "phid" => "USER_123"
					    }
					}
				)
				event = Event.create! valid_attributes
				put :update, params: {id: event.to_param, event: valid_attributes}, xhr: true
				expect(assigns(:event)).to eq(event)
		    end

			it "redirects to the event" do
				allow(controller).to receive(:user_signed_in?).and_return(true)
		      	allow(controller).to receive(:get_oauth_user).and_return(
					{"result" => {
					      "phid" => "USER_123"
					    }
					}
				)
				event = Event.create! valid_attributes
				put :update, params: {id: event.to_param, event: valid_attributes}, xhr: true
				expect(response).to have_http_status(200)
			end
		end
	end

	describe "DELETE #destroy" do
		it "destroys the requested event" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
	      	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)
			event = Event.create! valid_attributes
			expect {
				delete :destroy, params: {id: event.to_param}, xhr:true
			}.to change(Event, :count).by(-1)
		end

		it "returns success code (200)" do
			allow(controller).to receive(:user_signed_in?).and_return(true)
	      	allow(controller).to receive(:get_oauth_user).and_return(
				{"result" => {
				      "phid" => "USER_123"
				    }
				}
			)
			event = Event.create! valid_attributes
			delete :destroy, params: {id: event.to_param}, xhr: true
			expect(response).to have_http_status(200)
		end
	end
end
